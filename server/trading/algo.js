var {
    Level, GainMode, LvlState, MarketEnv, Order, OrderType, AlgoState,
    price_add_perc, price_diff_perc, price_sub_perc, print_LVLs_info,
    sleepWithCondition, sleep, retry
} = require('./common');
var _ = require('lodash');
var ccxt = require('ccxt');

const DEBUG = false;

function get_new_price(market_env, type, lvl, recreate) {
    const MIN_PONG_DIFF_PERC = Math.max(market_env.cfg.MIN_PONG_DIFF_PERC, market_env.fee_maker * 2 * 100);

    const is_dust = volume => volume < market_env.cfg.DUST_CREATE_ON_TOP_VOL;

    var [orderbook, otherbook] = type === OrderType.BUY ? [market_env.orders_buy, market_env.orders_sell] :
                                                          [market_env.orders_sell, market_env.orders_buy];

    const price_at_top = () => {
        let [best, opposite_best] = [orderbook[0], otherbook[0]];
        if (is_dust(best.volume))
            return best.price;
        if (type === OrderType.BUY) {
            let new_price = best.price + market_env.price_precision;
            if (new_price >= opposite_best.price)
                return best.price;
            return new_price;
        } else {
            let new_price = best.price - market_env.price_precision;
            if (new_price <= opposite_best.price)
                return best.price;
            return new_price;
        }
    };

    if (!recreate) {
        if (lvl.state === LvlState.NONE)  // PING
            return price_at_top();
        // PONG
        let new_price = price_at_top();
        if (type === OrderType.BUY) {
            let diff_perc = price_diff_perc(lvl.ping_price, new_price);
            new_price = price_sub_perc(lvl.ping_price, Math.max(diff_perc, MIN_PONG_DIFF_PERC));
            new_price = market_env.fixPrice(new_price, 'DOWN');
        } else {
            let diff_perc = price_diff_perc(new_price, lvl.ping_price);
            new_price = price_add_perc(lvl.ping_price, Math.max(diff_perc, MIN_PONG_DIFF_PERC));
            new_price = market_env.fixPrice(new_price, 'UP');
        }
        return new_price;
    }


    // recreate

    let old_price = lvl.state === LvlState.PING ? lvl.ping_price : lvl.pong_price;

    const is_after = (price) => {
        if (type === OrderType.BUY)
            return price > old_price;
        return price < old_price;
    };

    let vol_after = _.reduce(orderbook, (sum, order) => is_after(order.price) ? sum + order.volume : sum, 0);
    if (lvl.state === LvlState.PING) {
        if (vol_after < market_env.cfg.VOL_RECREATE_PING)
            return null;
        return price_at_top();
    }

    // PONG
    if (vol_after < market_env.cfg.VOL_RECREATE_PONG)
        return null;
    let new_price = price_at_top();
    if (type === OrderType.BUY)
        var diff_perc = price_diff_perc(lvl.ping_price, new_price);
    else
        diff_perc = price_diff_perc(new_price, lvl.ping_price);
    if (diff_perc < MIN_PONG_DIFF_PERC)
        return null;
    return new_price;
}


class InSpreadAlgo {
    constructor(ex/*, extra_ex*/, symbol, cfg, log) {
        this.cfg = cfg;
        this.log = log;
        this.state = AlgoState.INIT;
        this.ccxt = new ccxt[ex.name](ex.init);
        // this.extra_ccxt = new ccxt[extra_ex.name](extra_ex.init);

        this.LVLs = [];

        this.finished_LVLs = [];
        this.filled_orders = [];
        this.counts = { 'ping': 0, 'pong': 0, 'trade': 0 };

        this.profit = 0;

        this.symbol = symbol;
        this.exchange = ex.name;
        this.market_env = new MarketEnv(this.cfg);
        // this.extra_market = new MarketEnv(this.cfg);

        this.sleep = sleepWithCondition(() => this.state === AlgoState.STOP);
    }

    toJSON() {
        let bC = this.market_env.base_currency, pC = this.market_env.partner_currency;
        return {
            cfg: this.cfg,
            counts: this.counts,
            symbol: this.symbol,
            market: this.symbol,
            base_currency: bC,
            partner_currency: pC,
            exchange: this.exchange,
            profit: this.profit,
            LVLs: this.LVLs.map(lvl => lvl.toJSON()),
            finished_LVLs: this.finished_LVLs.map(lvl => lvl.toJSON()),
            filled_orders: this.filled_orders,
            balance_before: _.pick(this.balance_before, [bC, pC]),
            balance: _.pick(this.balance, [bC, pC]),
            state: this.state
        };
    }
    static fromJSON(state, ex/*, extra_ex*/, log) {
        let algo = new InSpreadAlgo(ex/*, extra_ex*/, state.symbol, state.cfg, log);
        Object.assign(algo.counts, state.counts);
        Object.assign(algo.filled_orders, state.filled_orders);
        // algo.filled_orders = state.filled_orders;
        algo.state = state.state;
        algo.profit = state.profit;
        algo.LVLs = state.LVLs.map(st => Level.fromJSON(st, algo.market_env));
        algo.finished_LVLs = state.finished_LVLs.map(st => Level.fromJSON(st, algo.market_env));
        algo.balance_before = state.balance_before;
        algo.balance = state.balance;
        return algo;
    }


    async run(cbOnChanged, cbOnFinished) {
        let log = this.log;
        this.state = AlgoState.RUN;
        this.stopped = false;
        cbOnChanged && cbOnChanged(this);

        while (this.state !== AlgoState.STOP)
            try {
                await retry(() => this.market_env.init(this.ccxt, this.symbol));
                // await this.extra_market.init(this.extra_ccxt, this.symbol);

                for (let lvl of this.LVLs)
                    log.info(lvl.str());

                // this.ccxt.cancel_orders(this.market_env.symbol)
                await sleep(2000);

                if (_.isEmpty(this.balance_before))
                    this.balance_before = this.balance = await retry(() => this.ccxt.fetch_balance());
                let b = this.balance_before;
                let bC = this.market_env.base_currency, pC = this.market_env.partner_currency;
                log.info(`balance ${pC} before: ${b[pC]["total"].toFixed(6)} (${b[pC]["free"].toFixed(6)}+${b[pC]["used"].toFixed(6)})`);
                log.info(`balance ${bC} before: ${b[bC]["total"].toFixed(6)} (${b[bC]["free"].toFixed(6)}+${b[bC]["used"].toFixed(6)})`);
                break;
            } catch (e) {
                log.error(e)
            }

        let cnt = 0;
        while (this.loop_condition() && this.state !== AlgoState.STOP) {
            try {

                this.market_env.orders = await retry(() => this.ccxt.fetch_order_book(this.market_env.symbol));
                if (this.LVLs.length)
                    this.market_env.my_orders = await retry(() => this.ccxt.fetch_open_orders(this.market_env.symbol));

                if (cnt++ % 100 === 0)  // update low/high 24h
                    await retry(() => this.market_env.updateMarket(this.ccxt));

                await this.updateLVLs();
                // if (this.market_env.is_price_changed || this.market_env.is_spread_changed || this.market_env.is_top_volume_changed)
                if (this.market_env.is_price_changed)
                    this.print_price();
                for (let lvl of this.LVLs)
                    if (await this.order_for_lvl(lvl)) {
                        print_LVLs_info(this.LVLs, log);
                        log.info(`CNT ping ${this.counts.ping}  pong ${this.counts.pong}  trades ${this.counts.trade}`);
                        if (this.finished_LVLs.length) {
                            log.info('Finished LVLs:');
                            for (let lvl of this.finished_LVLs)
                                log.info(lvl.str());
                        }
                        if (this.LVLs.length) {
                            log.info('Current LVLs:');
                            for (let lvl of this.LVLs)
                                log.info(lvl.str());
                        }
                        await this.sleep(5 * 1000);
                        this.market_env.my_orders = await retry(() => this.ccxt.fetch_open_orders(this.market_env.symbol));
                        let openOrders = this.market_env.my_orders.map(o => `${o.id}:${o.filled}:${o.remaining}`).join();
                        log.info('Open orders:' + openOrders);
                        break;  // need? some delay between creating orders
                    }
                cbOnChanged && cbOnChanged(this);
                if (this.state === AlgoState.STOP)
                    break;
                // await sleep(this.cfg.SLEEP_DELAY * 1000);
                await this.sleep(this.cfg.SLEEP_DELAY * 1000);
            } catch (e) {
                log.error(e);
                await this.sleep(15 * 1000);
            }

        }

        if (this.state === AlgoState.STOP) {
            log.info('Stopped');
            this.stopped = true;
            cbOnChanged && cbOnChanged(this);
            return;
        }

        this.state = AlgoState.FINISH;
        cbOnChanged && cbOnChanged(this);
        await this.on_finished();
        log.info('LOOP finished');
        cbOnFinished && cbOnFinished(this);
    }

    print_price() {
        let best_buy = this.market_env.best_buy.price, best_sell = this.market_env.best_sell.price,
            spread = this.market_env.spread_perc.toFixed(2),
            spread_buy = this.market_env.spread_buy.price, spread_sell = this.market_env.spread_sell.price,
            wide_spread = this.market_env.spread_wide_perc.toFixed(2),
            spread_buy_perc = this.market_env.spread_buy_perc.toFixed(2), spread_sell_perc = this.market_env.spread_sell_perc.toFixed(2),
            spread_buy_gap = this.market_env.spread_buy_gap_perc.toFixed(2), spread_sell_gap = this.market_env.spread_sell_gap_perc.toFixed(2),
            VOI = this.market_env.VOI.toFixed(4), OIR = this.market_env.OIR.toFixed(4), low = this.market_env.low, high = this.market_env.high;
        // let extra_buy = this.extra_market.best_buy.price, extra_sell = this.extra_market.best_sell.price,
        //     extra_price = this.extra_market.price.toFixed(8),
        //     price = this.market_env.price.toFixed(8),
        //     extra_spread = this.extra_market.spread_perc.toFixed(2);

        this.log.info(
            // `Price -> [${best_buy}, ${best_sell}] ${spread}% [${spread_buy}, ${spread_sell}] ${wide_spread}%  ${price} EX ${extra_price} [${extra_buy}, ${extra_sell}] ${extra_spread}% (B ${spread_buy_perc}%/${spread_buy_gap}% - S ${spread_sell_perc}%/${spread_sell_gap}%)  VOI ${VOI}  OIR ${OIR}  low/high [${low}, ${high}]`
            `Price -> [${best_buy}, ${best_sell}] ${spread}% [${spread_buy}, ${spread_sell}] ${wide_spread}%  (B ${spread_buy_perc}%/${spread_buy_gap}% - S ${spread_sell_perc}%/${spread_sell_gap}%)  VOI ${VOI}  OIR ${OIR}  low/high [${low}, ${high}]`
        )

    }

    async updateLVLs() {
        const lvl_in_bounds = (lvl) => {
            if (lvl.state !== LvlState.PONG)
                return true;
            let mid = (lvl.buy_price + lvl.sell_price) / 2;
            if (lvl.is_buy)
                return mid < this.market_env.best_sell.price;
            else
                return mid > this.market_env.best_buy.price;
        };

        const MAX_LVL_NUM = 3;

        const add_lvl_condition = () => {
            let res = false;
            if (this.LVLs.length === 0)
                res = true;
            if (this.LVLs.length === 1)
                if (this.LVLs[0].state === LvlState.PONG)
                    res = true;
            if (res && this.LVLs.length < MAX_LVL_NUM) {
                // if (this.LVLs.length < MAX_LVL_NUM) {
                // if (this.LVLs.map(lvl_in_bounds).filter(val => !val).length > 0)  // <-> exist: lvl_in_bounds -> false
                //     res = true;
                // }
                res = this.create_ping_condition();
                if (res) {
                    // let buy = this.LVLs.filter(l => l.is_buy), sell = this.LVLs.filter(l => !l.is_buy);
                    //// exist not lvl_in_bounds && not exist lvl_in_bounds same type -> true for that type
                    let LVLs = this.LVLs.filter(l => l.type === res);
                    // if (LVLs.some(l => !lvl_in_bounds(l)))
                    if (LVLs.some(lvl_in_bounds))
                        res = false;
                    // if (LVLs.some(l => !lvl_in_bounds(l)) && !LVLs.some(lvl_in_bounds))
                }
            }

            if (res) {
                // res = this.create_ping_condition();
                if (res === OrderType.BUY) {
                    if (this.LVLs.filter(lvl => lvl.is_buy).length >= this.cfg.MAX_BUY_LVL_NUM)
                        res = false;
                } else if (res === OrderType.SELL)
                    if (this.LVLs.filter(lvl => !lvl.is_buy).length >= this.cfg.MAX_SELL_LVL_NUM)
                        res = false;
            }
            // if (this.LVLs.length >= MAX_LVL_NUM)
            //     res = false;
            // if (res) {
            //     let buy = this.LVLs.filter(l => l.is_buy), sell = this.LVLs.filter(l => !l.is_buy);
            //     // exist not lvl_in_bounds && not exist lvl_in_bounds same type -> true for that type
            //     let LVLs = res === OrderType.BUY ? buy : sell;
            //     if (!(LVLs.some(l => !lvl_in_bounds(l)) && !LVLs.some(lvl_in_bounds)))
            //         res = false;
            // }
            return res;
        };

        let type = add_lvl_condition();

        if (type === false)
        // if not this.create_ping_condition():
        // log.info(f'Wait for next lvl condition')
            return;

        if (type === OrderType.BUY) {
            if (!this.balance)
                this.balance = await retry(() => this.ccxt.fetch_balance());
            let balance = this.balance[this.market_env.partner_currency];
            let total = this.cfg.BUY_AMOUNT * this.market_env.best_buy.price;
            if (balance["free"] < total) {
                this.log.info(`Not enough ${this.market_env.partner_currency}: ${balance["free"]} < ${total}`);
                return;
            }
        }

        if (type === OrderType.BUY)
            var lvl = new Level(this.market_env, OrderType.BUY, this.cfg.BUY_AMOUNT, -Infinity, Infinity, this.cfg.GAIN_MODE);
        else
            lvl = new Level(this.market_env, OrderType.SELL, this.cfg.SELL_AMOUNT, -Infinity, Infinity, this.cfg.GAIN_MODE);
        // lvl = new Level(this.market_env, OrderType.BUY, 2.0);

        this.log.info('New LVL: ' + lvl.str());
        this.LVLs.push(lvl);
    }

    delLvl(i) {
        if (i < 0 || i >= this.LVLs.length)
            throw new Error('delLvl: Index out of bounds');
        let lvl = this.LVLs[i];
        this.log.info('Manually remove LVL: ' + lvl.str());
        if (lvl.state === LvlState.PING)
            this.counts.ping--;
        else if (lvl.state === LvlState.PONG)
            this.counts.pong--;
        this.LVLs.splice(i, 1);
    }
    // addLvl() {}

    async on_ping_pong_finished(lvl) {
        lvl.cnt += 1;
        this.profit += lvl.gain;
        this.log.info('LVL finished: ' + lvl.str());

        this.finished_LVLs.push(lvl);
        _.pull(this.LVLs, lvl);

        await sleep(2 * 1000);
        await this.print_balance_diff();
    }

    check_spread_for_ping() {
        return this.market_env.spread_perc > this.cfg.MIN_PING_SPREAD_PERC;
    }
    check_spread_for_pong() {
        return this.market_env.spread_perc > this.cfg.MIN_PONG_SPREAD_PERC;
    }
    create_ping_condition(type) {
        let ping_cnt = this.counts.ping;
        if (ping_cnt >= this.cfg.PING_NUM_LIMIT)
            return false;
        // return False if type is not None else []

        const buy_cond = () => {
            return (
                this.market_env.price < this.market_env.mid &&
                this.market_env.spread_perc > this.cfg.MIN_PING_SPREAD_PERC &&
                this.market_env.spread_buy_perc > this.cfg.MIN_PING_SPREAD_BUY_PERC &&
                this.market_env.spread_buy_gap_perc < this.cfg.SPREAD_BUY_GAP_PERC
            )
        };
        const sell_cond = () => {
            return this.market_env.price > this.market_env.mid &&
                this.market_env.spread_perc > this.cfg.MIN_PING_SPREAD_PERC &&
                this.market_env.spread_sell_perc > this.cfg.MIN_PING_SPREAD_SELL_PERC &&
                this.market_env.spread_sell_gap_perc < this.cfg.SPREAD_SELL_GAP_PERC
        };

        if (type === OrderType.BUY || type === OrderType.SELL)
            return type === OrderType.BUY ? buy_cond() : sell_cond();

        if (buy_cond())
            return OrderType.BUY;
        if (sell_cond())
            return OrderType.SELL;
        return false;
    }

    create_pong_condition(lvl) {
        if (this.market_env.spread_perc > this.cfg.MIN_PONG_SPREAD_PERC)
            return true;
        if (lvl.is_buy)
            var diff = price_diff_perc(this.market_env.best_sell.price, lvl.ping_price);
        else
            diff = price_diff_perc(lvl.ping_price, this.market_env.best_buy.price);
        return diff > this.cfg.MIN_PONG_SPREAD_PERC
    }

    loop_condition() {
        let trade_cnt = this.counts.trade, ping_cnt = this.counts.ping;
        let cond = trade_cnt < this.cfg.PING_NUM_LIMIT || trade_cnt < ping_cnt;
        return cond;
    }

    get balance() {
        if (!InSpreadAlgo.balances) InSpreadAlgo.balances = {};
        return InSpreadAlgo.balances[_.get(this, 'dbData.userId')];
    }
    set balance(val) {
        if (!InSpreadAlgo.balances) InSpreadAlgo.balances = {};
        InSpreadAlgo.balances[_.get(this, 'dbData.userId')] = val;
    }
    // available_balance(currency) {
    //     // ETH
    //     let userId = _.get(this, 'dbData.userId', -1);
    //     let algoList = _.chain(InSpreadAlgo.algoMap).filter(algo => _.get(algo, 'dbData.userId') === userId).value();
    //                                 // .filter(algo.state == AlgoState.RUN)
    //     for (let algo of algoList)
    //         for (let lvl of algo.LVLs)
    //             if (lvl.state === LvlState.PING && lvl.order_id)
    //     let reserved = 0;
    //     return this.balance[currency] - reserved;
    // }

    async print_balance_diff() {
        let balance = this.balance = await retry(() => this.ccxt.fetch_balance());
        let bC = this.market_env.base_currency, pC = this.market_env.partner_currency;

        this.log.info(`balance ${pC} after: ${balance[pC]["total"].toFixed(6)} (${balance[pC]["free"].toFixed(6)}+${balance[pC]["used"].toFixed(6)})`);
        this.log.info(`balance ${bC} after: ${balance[bC]["total"].toFixed(6)} (${balance[bC]["free"].toFixed(6)}+${balance[bC]["used"].toFixed(6)})`);

        let partner_diff = this.market_env.fixPrice(balance[pC]["total"] - this.balance_before[pC]["total"], 'DOWN');
        let base_diff = this.market_env.fixAmount(balance[bC]["total"] - this.balance_before[bC]["total"], 'DOWN');

        this.log.info(`${pC} diff: ${partner_diff}, ${bC} diff: ${base_diff}`);
    }

    print_orderbooks() {
        let oB = this.market_env.orders_buy, oS = this.market_env.orders_sell;
        let order2str = o => `${o.price}\t${o.amount}\t${o.volume}`;
        let str = `BUY book: ${oB.map(order2str)}\nSELL book: ${oS.map(order2str)}`;
        this.log.info(str);
    }

    async on_finished() {
        let log = this.log;
        for (let lvl of this.LVLs)
            log.info(lvl.str());

        let b = this.balance_before;
        let bC = this.market_env.base_currency, pC = this.market_env.partner_currency;
        log.info(`balance ${pC} before: ${b[pC]["total"].toFixed(6)} (${b[pC]["free"].toFixed(6)}+${b[pC]["used"].toFixed(6)})`);
        log.info(`balance ${bC} before: ${b[bC]["total"].toFixed(6)} (${b[bC]["free"].toFixed(6)}+${b[bC]["used"].toFixed(6)})`);

        // this.ccxt.cancel_orders(this.market_env.symbol)
        for (let lvl of this.LVLs)
            if (lvl.state === LvlState.PING || lvl.state === LvlState.PONG) {
                await this.ccxt.cancel_order(lvl.order_id, this.market_env.symbol, {'type': lvl.state_order_type});
                log.info(`CANCEL(${lvl.order_id}) ${lvl.state}`);
                await sleep(2 * 1000);
            }
        await sleep(10 * 1000);

        await this.print_balance_diff();

        let estimated_profit = _.sum(this.finished_LVLs.map(lvl => lvl.profit));
        log.info(`Estimated profit: ${estimated_profit}`);

        log.info('Finished LVLs:');
        for (let lvl of this.finished_LVLs)
            log.info(lvl.str())
        log.info(`Profit: ${this.profit}`);
    }

    async stop() {
        this.state = AlgoState.STOP;
        let sleepStop = sleepWithCondition(() => this.stopped);
        await sleepStop(20 * 1000);
    }

    updateCfg(cfg) {
        Object.assign(this.cfg, cfg);
        this.LVLs.filter(l => l.state === LvlState.NONE).forEach(lvl => {
            lvl.ping_amount = lvl.is_buy ? cfg.BUY_AMOUNT : cfg.SELL_AMOUNT;
        });
    }

    async order_for_lvl(lvl) {
        let changed = false;
        let market_env = this.market_env, log = this.log;

        if (lvl.state === LvlState.PING || lvl.state === LvlState.PONG)
            if (market_env.is_order_filled(lvl.order_id)) {
                if (!_.includes(this.filled_orders, lvl.order_id)) {
                    log.info(`order ${lvl.order_id} filled`);
                    this.filled_orders.push(lvl.order_id);
                }
                if (lvl.state === LvlState.PING) { // PONG?

                    if (this.create_pong_condition(lvl)) {
                        lvl.pong_price = get_new_price(market_env, lvl.pong_type, lvl, false);

                        let amount = Math.max(lvl.pong_amount, market_env.min_amount);
                        let price = lvl.pong_price;
                        let transaction;

                        for (let i = 0; i < 2; i++) {
                            try {
                                transaction = await this.ccxt.create_order(
                                    market_env.symbol, 'limit', lvl.pong_type,
                                    amount + market_env.amount_p / 3, price + market_env.price_p / 3
                                );
                                break;
                            } catch (e) {
                                log.error(e);
                                // Error: kucoin {"success":false,"code":"NO_BALANCE","msg":"Insufficient balance: XRB","timestamp":1531324722879,"data":{"coinType":"XRB","expect":19.98,"actual":8.8e-7}}
                                let m = e.message.match(/Insufficient balance:/);
                                if (!m) throw e;
                                amount -= market_env.amount_p;
                                log.info(`try amount ${amount}`);
                            }
                        }
                        if (!transaction) throw new Error('Cannot create PONG tx');
                        lvl.order_id = transaction['id'];
                        lvl.state = LvlState.PONG;
                        this.counts.pong++;
                        log.info('{0} {1} {2}@{3} order id: {4} (price now - [{5}, {6}])'.format(
                            lvl.state,
                            lvl.pong_type,
                            amount, price,
                            lvl.order_id,
                            market_env.best_buy.price, market_env.best_sell.price
                        ));
                        changed = true;
                    }
                } else {  // PONG
                    lvl.state = LvlState.NONE;
                    this.counts.trade++;
                    changed = true;
                    await this.on_ping_pong_finished(lvl);
                    return changed
                }

            } else {  // recreate?
                let type = lvl.state_order_type;
                let new_price = get_new_price(market_env, type, lvl, true);
                let old_price = lvl.state === LvlState.PING ? lvl.ping_price : lvl.pong_price;
                let order_info = market_env.order_info(lvl.order_id);

                if (new_price) {
                    if (!order_info)
                        throw new Exception(`${lvl.order_id} not in my_orders`);

                    await this.ccxt.cancel_order(lvl.order_id, market_env.symbol, {'type': type});
                    log.info(`CANCEL(${lvl.order_id}) ${old_price} -> ${new_price}, ${order_info["filled"]} filled, ${order_info["remaining"]} remaining`);


                    if (lvl.state === LvlState.PING) {
                        if (order_info["filled"] === 0) {  // to change p-p direction
                            lvl.state = LvlState.NONE;
                            _.pull(this.LVLs, lvl);
                            this.counts.ping--;
                            log.info('REMOVE LVL ' + lvl.str());

                            await sleep(4 * 1000);
                            await this.print_balance_diff();

                            return true;
                        }
                        lvl.ping_amount = order_info["filled"];

                        await sleep(4 * 1000);
                        await this.print_balance_diff();

                        return true;
                    }


                    // PONG
                    let ping_amount_by_pong = lvl.calc_ping_amount_by_pong(order_info["remaining"]);
                    let amount = lvl.calc_pong_amount(ping_amount_by_pong, new_price);
                    log.info(`PONG recalc: ${order_info["remaining"]} remaining, ping_amount_by_pong ${ping_amount_by_pong}, new amount ${amount}`);
                    amount = Math.max(amount, market_env.min_amount);

                    let transaction = await this.ccxt.create_order(
                        market_env.symbol, 'limit', type,
                        amount + market_env.amount_p / 3, new_price + market_env.price_p / 3
                    );
                    lvl.order_id = transaction['id'];
                    if (lvl.state === LvlState.PING)
                        lvl.ping_price = new_price;
                    else
                        lvl.pong_price = new_price;
                    log.info('RECREATE {0} {1} {2}@{3} order id: {4} (price now - [{5}, {6}])'.format(
                        lvl.state,
                        type,
                        amount, new_price,
                        lvl.order_id,
                        market_env.best_buy.price, market_env.best_sell.price
                    ));

                    await sleep(4 * 1000);
                    await this.print_balance_diff();

                    changed = true;
                }
            }

        if (lvl.state === LvlState.NONE)  // PING?
            if (market_env.is_ping_valuable(lvl) && this.create_ping_condition(lvl.type)) {

                if (DEBUG)
                    return false;

                lvl.ping_price = get_new_price(market_env, lvl.ping_type, lvl, false);

                let price = lvl.ping_price;
                let amount = Math.max(lvl.ping_amount, market_env.min_amount);

                this.print_orderbooks();

                let transaction = await this.ccxt.create_order(
                    market_env.symbol, 'limit', lvl.ping_type,
                    amount + market_env.amount_p / 3, price + market_env.price_p / 3
                );
                lvl.order_id = transaction['id'];
                lvl.state = LvlState.PING;
                this.counts.ping++;
                log.info('{0} {1} {2}@{3} order id: {4} (price now - [{5}, {6}])'.format(
                    lvl.state,
                    lvl.ping_type,
                    amount, price,
                    lvl.order_id,
                    market_env.best_buy.price, market_env.best_sell.price
                ));
                changed = true;
            }

        return changed
    }
}




module.exports = {
    get_new_price,
    InSpreadAlgo,
    sleep
};