require('./helpers');
var _ = require('lodash');

const OrderType = Object.freeze({
    BUY:   "BUY",
    SELL:  "SELL"
});

const LvlState = Object.freeze({
    PING:   "PING",
    PONG:   "PONG",
    NONE:   "NONE"
});

const GainMode = Object.freeze({
    IN_BASE_CURRENCY:     "IN_BASE_CURRENCY",
    IN_PARTNER_CURRENCY:  "IN_PARTNER_CURRENCY"
});

const AlgoState = Object.freeze({
    INIT: "INIT",
    STOP: "STOP",
    RUN:  "RUN",
    FINISH: "FINISH",
});


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
// sleep until either checkFunc() return TRUE or timeout
function sleepWithCondition(checkFunc, checkMs = 500) {
    return ms => new Promise(resolve => {
        let check = ms => setTimeout(() => {
            // time interval here = ms-checkMs => final condition is ms-checkMs <= 0
            checkFunc() || ms <= checkMs ? resolve() : check(ms-checkMs)
        }, Math.min(ms, checkMs));
        check(ms);
    });
}

async function retry(func, n = 5, sleepMs = 1000) {
    for (let i = 1; i <= n; i++)
        try {
            return await func();
        } catch (e) {
            if (i === n)
                throw e;
            console.log('retry', e);
            await sleep(sleepMs);
        }
}


class Level {
    constructor(market_env, type, order_amount, buy_price=-Infinity, sell_price=Infinity, gain_mod=GainMode.IN_BASE_CURRENCY) {
        this._state = LvlState.NONE;
        this._order_id = null;
        this._type = type;
        this._buy_price = buy_price;
        this._sell_price = sell_price;
        this._order_amount = order_amount;
        this.market_env = market_env;
        this.cnt = 0;
        this._gain_mod = gain_mod;
    }

    static fromJSON(state, market_env) {
        let lvl = new Level(market_env, state._type, state._order_amount, state._buy_price, state._sell_price, state._gain_mod);
        lvl.cnt = state.cnt;
        lvl._state = state._state;
        lvl._order_id = state._order_id;
        return lvl;
    }
    toJSON() {
        let obj = _.pick(this, ['_state', '_order_id', '_type', '_buy_price', '_sell_price', '_order_amount', 'cnt', '_gain_mod']);
        obj.gain = this.gain;
        return obj;
    }

    str() {
        return `LVL ${this.type} ${this.ping_amount} @ [${this.buy_price}, ${this.sell_price}] GAIN ${this.gain} (${this.gain_perc.toFixed(2)}%) * ${this.cnt} ---- state: ${this.state}`
    }

    get state()     { return this._state; }
    set state(val)  { return this._state = val; }

    get order_id()      { return this._order_id; }
    set order_id(val)   { return this._order_id = val; }

    get type() { return this._type; }
    set type(val) { return this._type = val; }

    get is_buy() { return this.type === OrderType.BUY; }

    get state_order_type() {
        if (this.is_buy)
            var type = this.state === LvlState.PING ? OrderType.BUY : OrderType.SELL;
        else
            type = this.state === LvlState.PONG ? OrderType.BUY : OrderType.SELL;
        return type
    }


    get buy_price()     { return this._buy_price; }
    set buy_price(val)  { return this._buy_price = val; }

    get sell_price()     { return this._sell_price; }
    set sell_price(val)  { return this._sell_price = val; }

    get ping_amount()     { return this._order_amount; }
    set ping_amount(val)  {
        return this._order_amount = this.market_env.fixAmount(val);
    }

    get pong_amount()     { return this.calc_pong_amount(); }

    // calc_pong_amount(ping_amount=this.ping_amount, pong_price=this.pong_price) {
    //
    //     if (!Number.isFinite(pong_price))
    //         return 0;
    //
    //     if (this._gain_mod === GainMode.IN_BASE_CURRENCY) {
    //         var amount = this.ping_price * ping_amount / pong_price;
    //         if (this.is_buy)
    //             amount = amount / (1 - this.market_env.fee/2);
    //         else
    //             amount = amount / (1 + this.market_env.fee/2);
    //     } else {
    //         if (this.is_buy)
    //             amount = ping_amount * (1 - this.market_env.fee);
    //         else
    //             amount = ping_amount / (1 - this.market_env.fee);
    //     }
    //     amount = this.market_env.fixAmount(amount);
    //     return amount
    // }
    calc_pong_amount(ping_amount=this.ping_amount, pong_price=this.pong_price) {

        if (!Number.isFinite(pong_price))
            return 0;

        if (this._gain_mod === GainMode.IN_BASE_CURRENCY) {
            var amount = this.ping_price * ping_amount / pong_price;
            if (this.is_buy)
                amount = amount / (1 - this.market_env.fee);
            else
                amount = amount / (1 + this.market_env.fee);
        } else {
            if (this.is_buy)
                amount = ping_amount * (1 - this.market_env.fee);
            else
                amount = ping_amount / (1 - this.market_env.fee);
        }
        amount = this.market_env.fixAmount(amount);
        return amount
    }

    calc_ping_amount_by_pong(pong_amount=this.pong_amount, pong_price=this.pong_price) {
        if (!Number.isFinite(pong_price))
            return 0;

        if (this._gain_mod === GainMode.IN_BASE_CURRENCY) {
            var ping_amount = pong_amount * pong_price / this.ping_price;
            if (this.is_buy)
                ping_amount *= (1 - this.market_env.fee);
            else
                ping_amount *= (1 + this.market_env.fee);
        } else
            if (this.is_buy)
                ping_amount = pong_amount / (1 - this.market_env.fee);
            else
                ping_amount = pong_amount * (1 - this.market_env.fee);

        ping_amount = this.market_env.fixAmount(ping_amount);
        return ping_amount
    }

    get ping_type()     { return this.type; }

    get pong_type()     {
        return this.type === OrderType.BUY ? OrderType.SELL : OrderType.BUY;
    }

    get ping_price() {
        return this.ping_type === OrderType.BUY ? this.buy_price : this.sell_price;
    }
    set ping_price(val) {
        if (this.is_buy)
            this.buy_price = val;
        else
            this.sell_price = val;
    }

    get pong_price() {
        return this.pong_type === OrderType.BUY ? this.buy_price : this.sell_price;
    }
    set pong_price(val)  {
        if (this.pong_type === OrderType.BUY)
            this.buy_price = val;
        else
            this.sell_price = val;
    }

    get gain() {
        if (this._gain_mod === GainMode.IN_BASE_CURRENCY) {
            if (this.is_buy)
                var gain = Math.abs(this.ping_amount * (1 - this.market_env.fee) - this.pong_amount);
            else
                gain = Math.abs(this.ping_amount - this.pong_amount * (1 - this.market_env.fee));
            gain = this.market_env.fixAmount(gain);
        } else {
            if (!Number.isFinite(this.sell_price) || !Number.isFinite(this.buy_price))
                return 0;
            let buy_total = this.buy_price * this.ping_amount, sell_total = this.sell_price * this.ping_amount;
            let fee_buy = buy_total * this.market_env.fee;
            let fee_sell = sell_total * this.market_env.fee;
            let fee = fee_buy + fee_sell;
            gain = sell_total - buy_total - fee;
            gain = this.market_env.fixPrice(gain);
        }
        return gain
    }

    get gain_perc() {
        if (!Number.isFinite(this.sell_price) || !Number.isFinite(this.buy_price))
            return 0;
        return price_diff_perc(this.sell_price, this.buy_price)
    }

    get profit() {
        return this.gain * this.cnt;
    }

    // checkAmountProfitability() {
    //     return true;
    //     let ping_price = this.market_env.best_buy.price;
    //     let pong_price;
    //     let pong_amount = this.calc_pong_amount(this.ping_amount, pong_price);
    // }
}

function precision_in_decimal(val) { return +`1e-${val}`; }

class Order {
    constructor(market_env, type=OrderType.BUY, price=0, amount=0) {
        this.market_env = market_env;
        this.type = type;
        this.price = price;
        this.amount = amount;
    }

    get price() { return this._price; }
    set price(val) { return this._price = this.market_env.fixPrice(val) }

    get amount() { return this._amount; }
    set amount(val) { return this._amount = this.market_env.fixAmount(val) }

    get volume() { return this._amount * this._price; }

    toJSON() {
        return _.pick(this, ['type', '_price', '_amount']);
    }
    static fromJSON(state, market_env) {
        return new Order(market_env, state.type, state._price, state._amount);
    }
}



let lagged_market_envs = [];
const lagged_market_envs_MAXLEN = 5;

class MarketEnv {
    constructor(cfg) {
        this.cfg = cfg;
        this._my_orders = [];
    }

    toJSON() {
        return {
            symbol: this.symbol,
            base_currency: this.base_currency,
            partner_currency: this.partner_currency,
            exchange: this.exchange,
            market: this.symbol,
            fee: this.fee,
            high: this.high,
            low: this.low,
            min_amount: this.min_amount,
            price_precision: this.price_precision,
            amount_precision: this.amount_precision,
            OIR: this.OIR,
            VOI: this.VOI,
            spread_sell: this.spread_sell,
            spread_buy: this.spread_buy,
            orders_buy: this.orders_buy,
            orders_sell: this.orders_sell,
            my_orders: this.my_orders,
            spread_perc: this.spread_perc,
            spread_wide_perc: this.spread_wide_perc,
            spread_buy_perc: this.spread_buy_perc,
            spread_sell_perc: this.spread_sell_perc,
            spread_buy_gap_perc: this.spread_buy_gap_perc,
            spread_sell_gap_perc: this.spread_sell_gap_perc,
            tick: this.tick,
            marketInfo: this.marketInfo
        };
    }


    async init(ccxt, symbol) {
        this.symbol = symbol;

        if (!ccxt) return;

        this.exchange = ccxt.name.toLowerCase();

        this.tick = await ccxt.fetch_ticker(symbol);
        let market = this.marketInfo = await ccxt.market(symbol);
        this.base_currency = market['base'];
        this.partner_currency = market['quote'];
        this.fee = Number(market['maker']);  // TODO: sell_fee -> maker_fee
        this.high = Number(this.tick['high']);
        this.low = Number(this.tick['low']);
        this.min_amount = market.limits.amount.min;
        this.price_precision = market.precision.price;
        this.amount_precision = market.precision.amount;

        if (this.exchange === 'kucoin') {
            this.price_precision = ccxt.currencies[this.partner_currency]["precision"];
            this.amount_precision = ccxt.currencies[this.base_currency]["precision"];

            // if (['KEY', 'POLY'].includes(this.base_currency))
            //     this.min_amount = 1.0;
            // if (['XRB'].includes(this.base_currency))
            //     this.min_amount = 0.1;

            try {
                let transaction = await ccxt.create_order(
                    this.symbol, 'limit', 'BUY',
                    this.amount_p, this.price_p*10
                );
                throw new Error();
            } catch (e) {
                let m = e.message.match(/Min amount each order:([\d.]+)/);
                if (!m)
                    throw new Error('min_amount????')
                this.min_amount = +m[1];
                console.log(`${this.symbol} min_amount`, this.min_amount);
            }

            // if (!this.min_amount)
            //     throw new Error('min_amount????');

        } else if (this.exchange === 'okex') {
            this.price_precision = market["precision"]["price"];
            this.amount_precision = market["precision"]["amount"];
        }
    }

    async updateMarket(ccxt) {
        this.tick = await ccxt.fetch_ticker(this.symbol);
        this.high = Number(this.tick['high']);
        this.low = Number(this.tick['low']);
    }

    // async updateBalance(ccxt) {
    //     this.balance = await retry(() => this.ccxt.fetch_balance());
    // }

    get OIR() {
        if (!this._orders_buy)
            return 0;
        let buy_vol = this.best_buy.volume, sell_vol = this.best_sell.volume;
        let imbalance = (buy_vol - sell_vol) / (buy_vol + sell_vol);
        return imbalance
    }
    get is_buy_heavy() { return this.OIR > 1/3 }
    get is_sell_heavy() { return this.OIR < -1/3 }

    get prev_env() {
        if (!(this in lagged_market_envs))
            if (lagged_market_envs.length > 0)
                return lagged_market_envs[lagged_market_envs.length-1];  // for current (last) instance
            else
                return null;
        let i = lagged_market_envs.indexOf(this);
        if (i === 0)
            return null;
        return lagged_market_envs[i-1];
    }

    get VOI() {
        if (!this._orders_buy || !this.prev_env || !this.prev_env._orders_buy)
            return 0;

        let _orders_buy1 = this.prev_env._orders_buy;
        let _orders_sell1 = this.prev_env._orders_sell;
        let best_buy = this.best_buy, best_sell = this.best_sell;
        let best_buy1 = _orders_buy1[0], best_sell1 = _orders_sell1[0];

        let P_buy = best_buy.price, P_sell = best_sell.price;
        let P_buy1 = best_buy1.price, P_sell1 = best_sell1.price;
        let V_buy = best_buy.volume, V_sell = best_sell.volume;
        let V_buy1 = best_buy1.volume, V_sell1 = best_sell1.volume;

        if (P_buy < P_buy1)
            var dV_buy = 0;
        else if (P_buy === P_buy1)
            dV_buy = V_buy - V_buy1;
        else
            dV_buy = V_buy;

        if (P_sell < P_sell1)
            var dV_sell = V_sell;
        else if (P_sell === P_sell1)
            dV_sell = V_sell - V_sell1;
        else
            dV_sell = 0;

        return dV_buy - dV_sell;
    }

    get price_precision()     { return this._price_precision; }
    set price_precision(val)  { return this._price_precision = val; }
    get price_p()     { return precision_in_decimal(this.price_precision); }

    get target_hi()     { return this._target_hi; }
    set target_hi(val)  { return this._target_hi = val; }

    get target_lo()     { return this._target_lo; }
    set target_lo(val)  { return this._target_lo = val; }

    get target_mid()     { return (this._target_hi + this._target_lo) / 2; }

    get fee_maker()     { return this._fee_maker; }
    set fee_maker(val)  { return this._fee_maker = val; }

    get fee_taker()     { return this._fee_taker; }
    set fee_taker(val)  { return this._fee_taker = val; }

    get fee()     { return this._fee_maker; }
    set fee(val)  { return this.fee_taker = this.fee_maker = val }

    get high()     { return this._high; }
    set high(val)  { return this._high = val; }

    get low()     { return this._low; }
    set low(val)  { return this._low = val; }

    get mid()     { return (this.low + this.high) / 2; }

    get min_amount()     { return this._min_amount; }
    set min_amount(val)  { return this._min_amount = val; }

    get price()     { return (this.best_buy.price + this.best_sell.price) / 2; }

    get best_buy()     { return this._orders_buy ? this._orders_buy[0] : new Order(this); }
    get best_sell()     { return this._orders_sell ? this._orders_sell[0] : new Order(this); }

    get spread_sell() {
        let orders = this.omit_dust_orders(this._orders_sell, this.cfg.SPREAD_SELL_DUST_VOL);
        if (!orders.length)
            return new Order(this);
        return orders[0];
    }
    get spread_buy() {
        let orders = this.omit_dust_orders(this._orders_buy, this.cfg.SPREAD_BUY_DUST_VOL);
        if (!orders.length)
            return new Order(this);
        return orders[0];
    }

    omit_dust_orders(orders, dust_volume) {
        if (!orders || !orders.length)
            return [];
        let result_orders = orders.slice();
        let vol = 0;
        while (result_orders.length > 0) {
            let order = result_orders[0];
            vol += order.volume;
            if (vol > dust_volume)
                break;
            _.pull(result_orders, order);
        }
        return result_orders;
    }

    get orders() { return [...this._orders_buy, ...this._orders_sell] }
    get orders_buy()     { return this._orders_buy }
    get orders_sell()     { return this._orders_sell }
    set orders(orders)     {
        lagged_market_envs.push(_.cloneDeep(this));
        if (lagged_market_envs.length > lagged_market_envs_MAXLEN)
            lagged_market_envs.shift();

        this._orders_buy = [];
        this._orders_sell = [];
        for (let o of orders['bids'])
            this._orders_buy.push(new Order(this, OrderType.BUY, o[0], o[1]));
        for (let o of orders['asks'])
            this._orders_sell.push(new Order(this, OrderType.SELL, o[0], o[1]));

        this.tick.bid = this.best_buy.price;
        this.tick.ask = this.best_sell.price;
    }

    get is_top_volume_changed() {
        if (!this.prev_env || !this.prev_env._orders_buy)
            return true;
        if (this.best_buy.volume !== this.prev_env.best_buy.volume ||
            this.best_sell.volume !== this.prev_env.best_sell.volume)
            return true;
        return false;
    }
    get is_price_changed() {
        if (!this.prev_env)
            return true;
        if (this.best_buy.price !== this.prev_env.best_buy.price ||
            this.best_sell.price !== this.prev_env.best_sell.price)
            return true;
        return false;
    }
    get is_spread_changed() {
        if (this.prev_env)
            return true;
        if (this.spread_buy.price !== this.prev_env.spread_buy.price ||
            this.spread_sell.price !== this.prev_env.spread_sell.price)
            return true;
        return false;
    }

    get my_orders()     { return this._my_orders; }
    set my_orders(val)  { return this._my_orders = val; }

    is_order_filled(order_id) {
        return this.order_info(order_id) === false;
    }
    order_info(order_id) {
        for (let o of this.my_orders)
            if (o.id === order_id)
                return o;
        return false;
    }

    get symbol()     { return this._symbol; }
    set symbol(val)  { return this._symbol = val; }

    get amount_precision()     { return this._amount_precision; }
    set amount_precision(val)  { return this._amount_precision = val; }
    get amount_p()     { return precision_in_decimal(this.amount_precision); }

    is_ping_valuable(lvl) {
        if (lvl.is_buy)
            return lvl.ping_price < this.best_sell.price;  // price > lvl.ping_price
        return lvl.ping_price > this.best_buy.price;  // price < lvl.ping_price
    }

    get spread_perc() {
        // https://en.wikipedia.org/wiki/Bid–ask_spread
        if (!this._orders_buy || !this._orders_sell)
            var spread = 0;
        else
            spread = price_diff_perc(this.best_sell.price, this.best_buy.price);
        return spread
    }
    get spread_wide_perc() {
        if (!this._orders_buy || !this._orders_sell)
            var spread = 0;
        else
            spread = price_diff_perc(this.spread_sell.price, this.spread_buy.price);
        return spread
    }

    get spread_buy_perc() {
        return price_diff_perc(this.spread_sell.price, this.best_buy.price)
    }
    get spread_sell_perc() {
        return price_diff_perc(this.best_sell.price, this.spread_buy.price)
    }

    get spread_buy_gap_perc() {
        return price_diff_perc(this.best_buy.price, this.spread_buy.price)
    }
    get spread_sell_gap_perc() {
        return price_diff_perc(this.spread_sell.price, this.best_sell.price)
    }


    fixAmount(val, rounding) {
        if (rounding === 'UP')
            return Math.ceil10(val, -this.amount_precision);
        if (rounding === 'DOWN')
            return Math.floor10(val, -this.amount_precision);
        return Math.round10(val, -this.amount_precision);
    }
    fixPrice(val, rounding) {
        if (rounding === 'UP')
            return Math.ceil10(val, -this.price_precision);
        if (rounding === 'DOWN')
            return Math.floor10(val, -this.price_precision);
        return Math.round10(val, -this.price_precision);
    }
}

function print_LVLs_info(LVLs, log) {
    let buy_LVLs = LVLs.filter(lvl => lvl.type === OrderType.BUY);
    let buy_ping = buy_LVLs.filter(lvl => lvl.state === LvlState.PING);
    let buy_pong = buy_LVLs.filter(lvl => lvl.state === LvlState.PONG);
    let buy_active = [...buy_ping, ...buy_pong];
    let buy_nope = _.difference(buy_LVLs, buy_active);

    let sell_LVLs = LVLs.filter(lvl => lvl.type === OrderType.SELL);
    let sell_ping = sell_LVLs.filter(lvl => lvl.state === LvlState.PING);
    let sell_pong = sell_LVLs.filter(lvl => lvl.state === LvlState.PONG);
    let sell_active = [...sell_ping, ...sell_pong];
    let sell_nope = _.difference(sell_LVLs, sell_active);

    let buy_gain = _.sum(buy_LVLs.map(lvl => lvl.gain * lvl.cnt));
    let sell_gain = _.sum(sell_LVLs.map(lvl => lvl.gain * lvl.cnt));

    let buy_cnt = _.sum(buy_LVLs.map(lvl => lvl.cnt));
    let sell_cnt = _.sum(sell_LVLs.map(lvl => lvl.cnt));

    log.info(`BUY Levels: ${buy_LVLs.length}/${buy_active.length}/${buy_ping.length}/${buy_pong.length}/${buy_nope.length} total/active/PING/PONG/NOPE, profit ${buy_gain}, cnt ${buy_cnt}`);
    log.info(`SELL Levels: ${sell_LVLs.length}/${sell_active.length}/${sell_ping.length}/${sell_pong.length}/${sell_nope.length} total/active/PING/PONG/NOPE, profit ${sell_gain}, cnt ${sell_cnt}`);
}

function price_diff_perc(price_hi, price_lo) {
    return (price_hi - price_lo) / price_hi * 100;
}
function price_add_perc(price_lo, diff_perc) {
    let price_hi = price_lo / (1 - diff_perc/100);
    return price_hi
}
function price_sub_perc(price_hi, diff_perc) {
    let price_lo = price_hi * (1 - diff_perc/100);
    return price_lo
}


module.exports = {
    OrderType,
    LvlState,
    GainMode,
    AlgoState,
    Level,
    Order,
    MarketEnv,
    print_LVLs_info,
    price_diff_perc,
    price_add_perc,
    price_sub_perc,
    sleepWithCondition,
    sleep,
    retry,
};