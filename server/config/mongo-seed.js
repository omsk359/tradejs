'use strict';

var mongo = require('./mongo'),
    config = require('./config'),
    ObjectID = mongo.ObjectID;

/**
 * Populates the database with seed data.
 * @param overwrite Overwrite existing database even if it is not empty.
 */
async function seed(overwrite) {
  var count = await mongo.users.count({}, {limit: 1});
  if (overwrite || count === 0) {

    // first remove any leftover data in collections
    var collerrmsg = 'ns not found' /* indicates 'collection not found' error in mongo which is ok */;
    for (var collection in mongo) {
      if (mongo[collection].drop) {
        try {
          await mongo[collection].drop();
        } catch (err) {
          if (err.message !== collerrmsg) {
            throw err;
          }
        }
      }
    }

    // now populate collections with fresh data
    await mongo.counters.insert({_id: 'userId', seq: users.length});
    // await mongo.users.insert(users);
  }
}

// declare seed data
var users = [
  {
    _id: 1,
    email: '',
    password: config.app.pass,
    name: '',
    picture: ''
  },
];


// export seed data and seed function
seed.users = users;
module.exports = seed;
