'use strict';

var route = require('koa-route'),
    mongo = require('../config/mongo');
var _ = require('lodash');
var ws = require('../config/ws');
var ccxt = require('ccxt');
var ObjectID = mongo.ObjectID;
var moment = require('moment');

var { InSpreadAlgo, sleep } = require('../trading/algo');
var { AlgoState, retry, sleepWithCondition } = require('../trading/common');

// register koa routes
exports.init = function(app) {
    app.use(route.post('/api/ccxt/getBalances', getBalances));
    app.use(route.post('/api/ccxt/startTrading', startTrading));
    app.use(route.post('/api/ccxt/stopTrading', stopTrading));
    app.use(route.post('/api/ccxt/updateCfg', updateCfg));
    app.use(route.post('/api/ccxt/addLvl', addLvl));
    app.use(route.post('/api/ccxt/algoList', algoList));
    app.use(route.post('/api/ccxt/delAlgo', delAlgo));
    app.use(route.post('/api/ccxt/loadTradingLogViaWS', loadTradingLogViaWS));
    app.use(route.post('/api/ccxt/loadAlgoDataWS', loadAlgoDataWS));
    app.use(route.post('/api/ccxt/getMarkets', ccxtGetMarkets));
    app.use(route.post('/api/ccxt/getExchanges', ccxtGetExchanges));

    app.use(route.post('/api/ccxt/getMethod', ccxtMethod));
    app.use(route.post('/api/ccxt/tradingMethod', tradingMethod));

    app.use(route.post('/api/ccxt/bulkFetchOHLCV', bulkFetchOHLCV));

    app.use(route.post('/api/createTask', createTask));
    app.use(route.post('/api/taskList', getTaskList));
    app.use(route.post('/api/taskState', getTaskState));
    app.use(route.post('/api/taskDel', taskDel));
};

async function getTaskList(ctx) {

    let user = ctx.state.user;

    let result = await mongo.tasks.find({ userId: user.id }).toArray();
    console.log('getTaskList result', result);

    ctx.body = result;
}

async function taskDel(ctx) {

    let user = ctx.state.user;
    let { taskId } = ctx.request.body;

    let task = taskMap[taskId];
    if (task && task.toJSON().taskState === AlgoState.RUN) {
        task.stop();
        await sleepWithCondition(() => task.toJSON().taskState !== AlgoState.RUN);
    }

    let result = await mongo.tasks.deleteOne({ userId: user.id, _id: ObjectID(taskId) });
    console.log('taskDel result', result);

    ctx.body = result;
}

async function getTaskState(ctx) {

    let user = ctx.state.user;
    let { taskId } = ctx.request.body;
    let task = taskMap[taskId], { log } = task;
    log.info({ ...task.dbData, state: task.toJSON() });
}

async function createTask(ctx) {

    let user = ctx.state.user;
    let { type } = ctx.request.body;

    if (type === 'fetchOHLCV')
        await bulkFetchOHLCV(ctx);
}

function getTaskLogger(userId, taskId) {
    let log = msg => {
        console.log(msg);
        ws.notify([userId], `task.${taskId}`, msg);
    };
    return {
        info: log,
        error: err => log('Error: ' + (err.message || err)),
    }
}

function onTaskChanged(task) {
    let { userId, _id } = task.dbData, { log } = task;
    log.info({ ...task.dbData, state: task.toJSON() });
    saveTaskState(userId, _id);
}

async function saveTaskState(userId, taskId) {

    let task = taskMap[taskId], state = task.toJSON();

    let result = await mongo.tasks.updateOne(
        { _id: ObjectID(taskId), userId },
        { $set: { state } }
    );
    return result;
}

let taskMap = {};

async function bulkFetchOHLCV(ctx) {

    let user = ctx.state.user;
    let { exchange, markets, timeframe, startDate, limit, noCache } = ctx.request.body.params;
    startDate = moment.utc(startDate);

    let ex = new ccxt[exchange]();
    if (!ex.has.fetchOHLCV || !ex.timeframes || !(timeframe in ex.timeframes))
        throw new Error(`"${timeframe}" timeframe not available for ${exchange} exchange`);

    let dbData = { type: 'fetchOHLCV', params: ctx.request.body.params, userId: user.id };
    let insertDbResult = await mongo.tasks.insertOne(dbData);
    let taskId = dbData._id = insertDbResult.ops[0]._id.toString();

    let task = taskMap[taskId] = {
        dbData,
        toJSON: () => ({results, taskState}),
        stop: () => taskState = AlgoState.STOP,
        log: getTaskLogger(user.id, taskId)
    };
    let taskState = AlgoState.INIT;
    let results = [];

    ctx.body = { ...task.dbData, state: task.toJSON() };

    setTimeout(async () => {
        let { log } = task;
        taskState = AlgoState.RUN;

        for (let market of markets) {
            let info = { exchange, market, timeframe };
            let dbTimeseries;
            if (!noCache)
                dbTimeseries = await mongo.timeseries.findOne(info);
            if (dbTimeseries) {
                let m = timeframe.match(/(\d+)([a-zA-Z])/);
                if (!m) throw new Error(`"Timeframe ${timeframe}" is invalid`);
                // let endDate = startDate.add(m[1]*limit, m[2]);
                let dates = dbTimeseries.values.map(item => item[0]), datesInCache = [];
                let date = moment.utc(startDate), i;
                for (i = 0; i < limit; i++) {
                    if (!dates.includes(+date))
                        break;
                    datesInCache.push(+date);
                    date.add(m[1], m[2]);
                    if (date.isAfter(moment.utc())) {
                        i = limit;
                        break;
                    }
                }
                if (i == limit) {
                    results.push({...dbTimeseries, values:  dbTimeseries.values.filter(([time]) => datesInCache.includes(time)) });
                    onTaskChanged(task);
                    continue;
                }
            }
            await sleep(ex.rateLimit);
            // await sleep(5*1000);
            if (taskState === AlgoState.STOP) return;

            let result;
            try {
                let values = await retry(() => ex.fetchOHLCV(market, timeframe, +startDate, limit));
                if (taskState === AlgoState.STOP) return;

                if (!dbTimeseries) {
                    result = {...info, values};
                    await mongo.timeseries.insertOne(result);
                } else {
                    values = _.sortBy(_.uniqBy([...dbTimeseries.values, ...values], ([time]) => time), item => item[0]);
                    result = {...info, values};
                    await mongo.timeseries.updateOne(info, { $set: { values } });
                }
            } catch (e) {
                result = {...info, error: e.message};
            }
            results.push(result);
            onTaskChanged(task);
            // log({ nextResult: result });
        }
        taskState = AlgoState.FINISH;
        onTaskChanged(task);
    }, 0)
}

async function ccxtMethod(ctx) {

    let user = ctx.state.user;

    let { method, exchange, market, id, orderType, timeframe, startDate, limit } = ctx.request.body;

    let initExData = await exchangeInitData(user.id, [exchange]), result;
    let instance = new ccxt[exchange](initExData[exchange]);

    try {
        if (method === 'balance')
            result = await instance.fetchBalance();
        else if (method === 'orderbook')
            result = await instance.fetch_order_book(market);
        else if (method === 'myOrders')
            result = await instance.fetch_open_orders(market);
        else if (method === 'cancelOrder')
            result = await instance.cancel_order(id, market, {'type': orderType});
        else if (method === 'tick')
            result = await instance.fetch_ticker(market);
        else if (method === 'marketInfo')
            result = await instance.market(market);
        else if (method === 'fetchOHLCV') {
            if (!instance.has.fetchOHLCV || !(timeframe in instance.timeframes))
                throw new Error(`"${timeframe}" timeframe not available for ${exchange} exchange`);
            result = await instance.fetchOHLCV(market, timeframe, startDate, limit);
        }
        result = {
            exchange,
            market,
            result: result,
            error: null
        }
    } catch (e) {
        result = {
            exchange,
            market,
            result: null,
            error: e.message
        }
    }
    ctx.body = result;
}


async function tradingMethod(ctx) {

    let user = ctx.state.user;

    let { method, id, index } = ctx.request.body;

    let result = '';
    try {
        if (method === 'delLvl') {
            let algo = algoMap[id];
            if (_.get(algo, 'dbData.userId') !== user.id)
                throw new Error(`Wrong algoId: ${id}`);
            algo.delLvl(index);
            algo.onAlgoChanged(algo);
            // result = await instance.fetchBalance();
        } else if (method === 'loadLog') {

            result = await mongo.algo.findOne(
                { _id: ObjectID(id), userId: user.id },
                { logs: true }
            );
            result = result.logs;
        }
    } catch (e) {
        console.log('tradingMethod', e);
        result = { error: e.message };
    }

    console.log(`tradingMethod[${method}] result : `, result);
    ctx.body = result;
}



async function getBalances(ctx) {

    let user = ctx.state.user;

    let { exchanges = [] } = ctx.request.body;

    let initExData = await exchangeInitData(user.id);
    if (exchanges.length)
        initExData = _.pick(initExData, exchanges);
    let ccxtArr = _.map(initExData, ({apiKey, secret}, name) => new ccxt[name]({ apiKey, secret }));

    let balances = await Promise.all(ccxtArr.map(ex => ex.fetchBalance()).map(p => p.catch(e => e)));

    let result = balances.map((r, i) => ({
        exchange: ccxtArr[i].id,
        result: r,
        error: (r instanceof Error) ? r.message : null
    }));

    console.log('balances result : ', result);
    ctx.body = result;
}

async function ccxtGetMarkets(ctx) {

    let { exchanges } = ctx.request.body;

    let matkets = await Promise.all(exchanges.map(name => {
        let ex = new ccxt[name]();
        return ex.load_markets();
    }).map(p => p.catch(e => e)));

    let result = matkets.map((r, i) => ({
        exchange: exchanges[i],
        result: r,
        error: (r instanceof Error) ? r.message : null
    }));

    console.log('ccxtGetMarkets result : ', result);
    ctx.body = result;
}

async function ccxtGetExchanges(ctx) {

    ctx.body = ccxt.exchanges;
}


function get_logger(userId, algoId) {
    let log = msg => {
        msg = `[${moment.utc().format('HH:mm')}] ${msg}`;
        algoMap[algoId].logBuf.push(msg);

        console.log(msg);
        ws.notify([userId], `ccxt.tradingLog.${algoId}`, msg);
    };
    return {
        info: log,
        error: err => log('Error: ' + (err.message || err)),
    }
}


async function algoList(ctx) {

    let user = ctx.state.user;

    let result = await mongo.algo.find({ userId: user.id }, { logs: false }).toArray();
    console.log('algoList result', result);

    ctx.body = result;
}

async function delAlgo(ctx) {

    let user = ctx.state.user;
    let algoId = ctx.request.body.id;

    let result = await mongo.algo.deleteOne({ _id: ObjectID(algoId), userId: user.id });
    console.log('delAlgo result', result);

    ctx.body = result;
}

async function loadAlgoDataWS(ctx) {

    let user = ctx.state.user;
    let algoId = ctx.request.body.id;

    let algo = algoMap[algoId];
    if (_.get(algo, 'dbData.userId') !== user.id)
        throw new Error(`Wrong algoId: ${algoId}`);

    algo && algo.onAlgoChanged(algo);

    ctx.body = '';
}

async function algoStopAll(ctx) {

    let user = ctx.state.user;

    let result = await mongo.algo.find({ userId: user.id }, { logs: false }).toArray();

    result.forEach(algoData => {
        let algo = algoMap[algoData._id];
        algo && algo.stop();
    });

    ctx.body = result;
}

async function loadTradingLogViaWS(ctx) {

    let { algoId } = ctx.request.body;

    let user = ctx.state.user;

    let algo = algoMap[algoId];
    if (_.get(algo, 'dbData.userId') !== user.id)
        throw new Error(`Wrong algoId: ${algoId}`);

    algo.logs.forEach(msg => {
        ws.notify([user.id], 'ccxt.tradingLog', msg);
    });

    ctx.body = '';
}

async function saveTradingState(userId, algoId) {

    let algo = algoMap[algoId], data = algo.toJSON();

    let result = await mongo.algo.updateOne(
        { _id: ObjectID(algoId), userId },
        { $set: { data }, $push: { logs: { $each: algo.logBuf } } }
    );
    algo.logBuf = [];
    return result;
}


async function exchangeInitData(userId, exchanges) {
    let result = await mongo.users.findOne(
        { _id: userId },
        { data: true }
    );
    let exchangesKeys = _.get(result, 'data.exchanges', {});
    if (!exchanges)
        exchanges = Object.keys(exchangesKeys);

    return exchanges.reduce((res, name) => {
        let exchangeKeys = _.find(exchangesKeys, (keys, exchange) => exchange === name);
        if (exchangeKeys)
            res[name] = {
                'apiKey': exchangeKeys.apiKey,
                'secret': exchangeKeys.secret
            };
        else
            res[name] = {};
        return res;
    }, {});
}


async function startTrading(ctx) {

    let user = ctx.state.user;

    let { algoId, cfg, exchange, market } = ctx.request.body;

    let algo, algoData;
    if (algoId) { // restore
        algoData = await mongo.algo.findOne({ _id: ObjectID(algoId), userId: user.id }, { logs: false });
        // let initExData = await exchangeInitData(user.id, [algoData.exchange, algoData.extraMarket]);
        // algo = InSpreadAlgo.fromJSON(algoData.data, {'name': algoData.exchange, 'init': initExData[algoData.exchange]}, {'name': algoData.extraMarket, 'init': initExData[algoData.extraMarket]});
        let ex = algoData.data.exchange;
        let initExData = await exchangeInitData(user.id, [ex]);
        algo = InSpreadAlgo.fromJSON(algoData.data, {'name': ex, 'init': initExData[ex]});
    } else {
        if (!cfg || !exchange || !market) return;
        console.log('startTrading', algoId, cfg);

        let numParams = _.keys(_.omit(cfg, ['GAIN_MODE']));
        cfg = _.mapValues(cfg, (n, key) => numParams.includes(key) ? +n : n);
        console.log('cfg', cfg);

        let initExData = await exchangeInitData(user.id, [exchange]);
        algo = new InSpreadAlgo({'name': exchange, 'init': initExData[exchange]}, market, cfg);

        algoData = { userId: user.id, type: 'spread', logs: [] };
        let results = await mongo.algo.insertOne({ ...algoData, data: algo.toJSON() });
        algoData._id = algoId = results.ops[0]._id.toString();
    }

    algoMap[algoId] = algo;
    algo.dbData = algoData;
    algo.log = get_logger(user.id, algoId);
    algo.logBuf = [];
    algo.onAlgoChanged = onAlgoChanged;

    algo.run(onAlgoChanged, () => {});
    await sleep(0);

    ctx.body = { ...algoData, data: algo.toJSON() };
}

function onAlgoChanged(algo) {
    let { userId, _id } = algo.dbData;
    saveTradingState(userId, _id);
    ws.notify([userId], `ccxt.tradingState.${_id}`, { ...algo.dbData, data: algo.toJSON() });
    ws.notify([userId], `ccxt.tradingMarketState.${_id}`, algo.market_env.toJSON());
}

async function autoRun() {
    let result = await mongo.algo.find({}, { logs: false }).toArray();
    console.log('autoRun result', result);

    result.forEach(async algoData => {
        // restore
        let ex = algoData.data.exchange;
        let initExData = await exchangeInitData(algoData.userId, [ex]);
        let algo = InSpreadAlgo.fromJSON(algoData.data, {'name': ex, 'init': initExData[ex]});

        algoMap[algoData._id] = algo;
        algo.dbData = _.omit(algoData, 'data');
        algo.log = get_logger(algoData.userId, algoData._id);
        algo.logBuf = [];
        algo.onAlgoChanged = onAlgoChanged;

        // if (algoData.data.state === AlgoState.RUN)
        //     algo.run(onAlgoChanged, () => {});
        await sleep(_.random(0, 15000));
        if (algoData.data.state === AlgoState.RUN)
            algo.run(onAlgoChanged, () => {});
    });
    // result.filter(algo => algo.data.state === AlgoState.RUN).forEach(async algoData => {
    //     // restore
    //     let ex = algoData.data.exchange;
    //     let initExData = await exchangeInitData(algoData.userId, [ex]);
    //     let algo = InSpreadAlgo.fromJSON(algoData.data, {'name': ex, 'init': initExData[ex]});
    //
    //     algoMap[algoData._id] = algo;
    //     algo.dbData = _.omit(algoData, 'data');
    //     algo.log = get_logger(algoData.userId, algoData._id);
    //     algo.logBuf = [];
    //     algo.onAlgoChanged = onAlgoChanged;
    //
    //     algo.run(onAlgoChanged, () => {});
    // });
}
exports.autoRun = autoRun;


async function stopTrading(ctx) {

    let user = ctx.state.user;
    let algoId = ctx.request.body.id, algo = algoMap[algoId];

    if (_.get(algo, 'dbData.userId') !== user.id)
        throw new Error(`Wrong algoId: ${algoId}`);
    console.log('stopTrading', algoId);


    await algo.stop();

    ctx.body = { ...algo.dbData, data: algo.toJSON() };
}


async function addLvl(ctx) {

    let user = ctx.state.user;
    let algoId = ctx.request.body.id, algo = algoMap[algoId];
    console.log('addLvl', algoId);

    if (_.get(algo, 'dbData.userId') !== user.id)
        throw new Error(`Wrong algoId: ${algoId}`);

    algoMap[algoId].addLvl();

    ctx.body = '';
}

// algoId => algo instance
let algoMap = {};
InSpreadAlgo.algoMap = algoMap;
// exports.algoMap = algoMap;

async function updateCfg(ctx) {

    let user = ctx.state.user;

    let { algoId, cfg } = ctx.request.body;
    console.log('updateCfg', algoId);

    let algo = algoMap[algoId];

    if (_.get(algo, 'dbData.userId') !== user.id)
        throw new Error(`Wrong algoId: ${algoId}`);

    cfg = _.mapValues(cfg, n => +n);
    console.log('cfg', cfg);

    algo.updateCfg(cfg);

    ctx.body = '';
}