'use strict';

/**
 * Users controller for user profile relation operations.
 */

var route = require('koa-route'),
    mongo = require('../config/mongo'),
    ObjectID = mongo.ObjectID,
    config = require('../config/config'),
    jwt = require('jsonwebtoken');

// register koa routes
exports.init = function (app) {
    app.use(route.post('/api/users', createUser));

    app.use(route.post('/api/user/saveData', saveUserData));
    app.use(route.post('/api/user/getData', getUserData));
};



/**
 * Creates a new user.
 */
async function createUser(ctx) {
    // todo: check user role === 'admin' when role system is ready
    // we need to validate user body with node-validator here not to save junk data in the database..
    var user = ctx.request.body;

    // get the latest userId+1 as the new user id
    // this is exceptional to user creation as we want user ids to be sequential numbers and not standard mongo guids
    user._id = await mongo.getNextSequence('userId');
    var results = await mongo.users.insertOne(user);

    ctx.status = 201;
    ctx.body = {id: results.ops[0]._id};
}




async function saveUserData(ctx) {
    // todo: check user role === 'admin' when role system is ready
    // we need to validate user body with node-validator here not to save junk data in the database..
    var userData = ctx.request.body;
    console.log('saveUser: ', userData);

    let user = ctx.state.user, userId = new ObjectID(user.id);
    console.log('saveUser userId: ', userId, user);

    let result = await mongo.users.updateOne(
        { _id: user.id },
        { $set: { data: userData } }
    );
    console.log('saveUser updateOne result: ', result);

    ctx.body = userData;
}


async function getUserData(ctx) {

    let user = ctx.state.user, userId = new ObjectID(user.id);
    console.log('getUserData  : ', userId, user);

    let result = await mongo.users.findOne(
        { _id: user.id },
        { data: true, _id: false }
    );
    console.log('getUserData: ', result);

    ctx.status = 200;
    if (result/* && result.data*/)
        ctx.body = result.data;
    else
        ctx.body = '';
}