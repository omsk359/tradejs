# Bot for trading cryptocurrencies on many popular exchanges (100+).

**Back-end: Koa 2 (Node.js 9), MongoDB, JWT.**

**Front-end: Angular 6, Angular Material, RxJS, ngrx/Effects, WebSocket, lodash, flex-layout.**