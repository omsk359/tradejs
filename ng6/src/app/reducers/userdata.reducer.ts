import {UserDataActionsUnion, UserDataActionTypes} from "../actions";
import _ from 'lodash';

export interface TradingCfgState {
  [param: string]: any
}
export interface TradingParamsState {
  [name: string]: TradingCfgState
}
export interface UserDataState {
  tradingParams: TradingParamsState,
  exchanges: {
    [name: string]: {
      apiKey: string,
      secret: string
    }
  }
}
const userDataInitState: UserDataState = { tradingParams: {}, exchanges: {} };


export const defaultCfg = {
  GAIN_MODE: 'IN_PARTNER_CURRENCY',
  MIN_PONG_DIFF_PERC: 0.25,
  PING_NUM_LIMIT: 1,
  MAX_BUY_LVL_NUM: 2,
  MAX_SELL_LVL_NUM: 2,

  DUST_CREATE_ON_TOP_VOL: 0.2, VOL_RECREATE_PING: 0.2, VOL_RECREATE_PONG: 0.4,
  SPREAD_BUY_DUST_VOL: 0.2, SPREAD_SELL_DUST_VOL: 0.2,

  MIN_PING_SPREAD_PERC: 1.5,
  MIN_PONG_SPREAD_PERC: 1.0,
  MIN_PING_SPREAD_BUY_PERC: 2.0, MIN_PING_SPREAD_SELL_PERC: 2.0,

  SPREAD_BUY_GAP_PERC: 1.0, SPREAD_SELL_GAP_PERC: 1.0,

  BUY_AMOUNT: 2.0, SELL_AMOUNT: 5.0, // 5k ~ 0.1

  SLEEP_DELAY: 30
};


export function userDataReducer(state = userDataInitState, action: UserDataActionsUnion): UserDataState {
  switch (action.type) {
    case UserDataActionTypes.DataSet:
      if (!action.payload) return state;
      return {...action.payload, tradingParams: _.mapValues(action.payload.tradingParams, cfg => _.defaults(_.cloneDeep(cfg), defaultCfg)) };
    case UserDataActionTypes.TradingParamsSet:
    case UserDataActionTypes.TradingParamsAdd:
      let { name, cfg } = action.payload;
      return {...state, tradingParams: { ...state.tradingParams, [name]: _.defaults(_.cloneDeep(cfg), defaultCfg) }};
    case UserDataActionTypes.TradingParamsDel:
      return {...state, tradingParams: _.omit(state.tradingParams, [action.name])};
    case UserDataActionTypes.ExchangeAdd:
      let { exchange, apiKey, secret } = action.payload;
      return {...state, exchanges: { ...state.exchanges, [exchange]: { apiKey, secret } }};
    case UserDataActionTypes.ExchangeDel:
      return {...state, exchanges: _.omit(state.exchanges, [action.exchange])};

    default:
      return state;
  }
}
