import {TasksActionsUnion, TasksActionTypes} from "../actions";
import _ from 'lodash';

export interface TaskState {
  _id: string,
  type: string,
  params: any,
  state: any
}

export interface TasksState {
  [taskId: string]: TaskState;
}

export function tasksReducer(state: TasksState = {}, action: TasksActionsUnion): TasksState {
  switch (action.type) {
    case TasksActionTypes.TasksListSet:
      return action.payload.reduce((state, task) => ({...state, [task._id]: task}), {});

    case TasksActionTypes.TaskSet:
      return {...state, [action.payload._id]: action.payload };

    case TasksActionTypes.TaskDel:
      return _.omit(state, action.taskId);

    default:
      return state;
  }
}
