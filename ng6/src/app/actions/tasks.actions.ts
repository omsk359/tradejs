import {Action} from "@ngrx/store";

export enum TasksActionTypes {
  TaskCreate = '[Tasks] Create Request',
  // TaskAdd = '[Tasks] Add',
  TaskSet = '[Tasks] Set',
  TaskDel = '[Tasks] Delete',
  TaskDelRequest = '[Tasks] Delete Request',
  TasksListLoad = '[Tasks] List Load',
  TasksListSet = '[Tasks] List Set',
  TaskSubWS = '[Tasks] Task Subscribe WebSocket',
}
export class TasksListLoad implements Action {
  readonly type = TasksActionTypes.TasksListLoad;
  constructor() {}
}
export class TaskCreate implements Action {
  readonly type = TasksActionTypes.TaskCreate;
  constructor(public payload: any) {}
}
export class TaskSubWS implements Action {
  readonly type = TasksActionTypes.TaskSubWS;
  constructor(public taskId: string) {}
}
export class TaskSet implements Action {
  readonly type = TasksActionTypes.TaskSet;
  constructor(public payload: any) {}
}
export class TaskDelRequest implements Action {
  readonly type = TasksActionTypes.TaskDelRequest;
  constructor(public taskId: string) {}
}
export class TaskDel implements Action {
  readonly type = TasksActionTypes.TaskDel;
  constructor(public taskId: string) {}
}
export class TasksListSet implements Action {
  readonly type = TasksActionTypes.TasksListSet;
  constructor(public payload: any) {}
}

export type TasksActionsUnion =
  | TasksListLoad
  | TaskCreate
  | TaskSet
  | TaskDelRequest
  | TaskDel
  | TasksListSet;
