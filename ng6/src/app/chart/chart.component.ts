import {ApplicationRef, Component, ElementRef, OnInit, Renderer, Renderer2, ViewEncapsulation} from '@angular/core';
import {AppState, LoadState, OHLCVItemState} from "../reducers";
import {Observable} from "rxjs/internal/Observable";
import {CcxtActionTypes, ExchangesLoad, LoadReset, LoadSuccess, MarketInfoLoad, MarketsLoad} from "../actions";
import {Store} from "@ngrx/store";
import {map} from "rxjs/operators";
import {ApiService} from "../api.service";
import {Subscription} from "rxjs/internal/Subscription";
import _ from 'lodash';
import {OHLCVLoad} from "../actions";
import {OHLCVState} from "../reducers";
import * as ccxt from 'ccxt';

export const timeframeNames = [
  {value: '1m', name: '1 Minute'},
  {value: '3m', name: '3 Minutes'},
  {value: '5m', name: '5 Minutes'},
  {value: '15m', name: '15 Minutes'},
  {value: '30m', name: '30 Minutes'},
  {value: '1h', name: '1 Hour'},
  {value: '2h', name: '2 Hours'},
  {value: '4h', name: '4 Hours'},
  {value: '6h', name: '6 Hours'},
  {value: '8h', name: '8 Hours'},
  {value: '12h', name: '12 Hours'},
  {value: '1d', name: '1 Day'},
  {value: '3d', name: '3 Days'},
  {value: '1w', name: 'Week'},
  {value: '1M', name: 'Month'},
];

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  timeframes: {value: string, name: string}[] = [];
  timeframe: string;

  market: string;
  exchange: string;

  exchanges: string[] = [];
  markets: {[exchange: string]: string[]} = {};

  OHLCV$: Observable<OHLCVItemState[]>;

  exchangesLoad$: Observable<LoadState>;
  marketsLoad$: Observable<LoadState>;
  OHLCVLoad$: Observable<LoadState>;

  ccxtSub: Subscription;

  constructor(private el: ElementRef, private appRef: ApplicationRef, private store: Store<AppState>, private api: ApiService) { }

  loadOHLCV() {
    let startDate = +new Date();
    this.store.dispatch(new OHLCVLoad({exchange: this.exchange, market: this.market, timeframe: this.timeframe, startDate, limit: 10}));
    let market$ = this.api.selectMarket(this.exchange, this.market);
    this.OHLCV$ = market$.pipe(map(({OHLCV}) => OHLCV[this.timeframe]));
    this.OHLCVLoad$ = market$.pipe(map(({OHLCV}) => OHLCV.load));
  }

  ngOnInit() {
    this.loadExchanges();
    this.ccxtSub = this.api.ccxt$.subscribe(({exchanges, markets}) => {
      this.exchanges = exchanges;
      this.markets = _.mapValues(markets, markets => _.keys(markets));
    });
    this.exchangesLoad$ = this.api.loading$.pipe(map(data => data[CcxtActionTypes.ExchangesLoad]));
    this.marketsLoad$ = this.api.loading$.pipe(map(data => data[CcxtActionTypes.MarketsLoad]));
  }

  ngOnDestroy() {
    this.ccxtSub && this.ccxtSub.unsubscribe();
  }

  loadExchanges() {
    this.store.dispatch(new ExchangesLoad());
  }

  loadMarkets(exchange) {
    let ccxtExchange = new ccxt[exchange];
    this.timeframes = timeframeNames.filter(({value}) => value in ccxtExchange.timeframes);
    if (!this.timeframes.find(item => item.value === this.timeframe))
      this.timeframe = this.timeframes.length && this.timeframes[0].value;

    if (exchange in this.markets) {
      this.store.dispatch(new LoadSuccess(new MarketsLoad()));  // reset error
      return;
    }
    this.store.dispatch(new MarketsLoad([exchange]));
  }

}
