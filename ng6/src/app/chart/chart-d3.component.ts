import {
  ApplicationRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer,
  Renderer2,
  ViewEncapsulation
} from '@angular/core';
import * as d3 from 'd3';
import {AppState, LoadState, OHLCVItemState, OrderbookState} from "../reducers";
import {Observable} from "rxjs/internal/Observable";
import {CcxtActionTypes, ExchangesLoad, LoadSuccess, MarketInfoLoad, MarketsLoad} from "../actions";
import {Store} from "@ngrx/store";
import {map} from "rxjs/operators";
import {ApiService} from "../api.service";
import {Subscription} from "rxjs/internal/Subscription";
import _ from 'lodash';
import {OHLCVLoad} from "../actions";
import {OHLCVState} from "../reducers";

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-chart-d3',
  template: '',
  styleUrls: ['./chart-d3.component.css']
})
export class ChartD3Component {

  @Input('OHLCV')
  set setOHLCV(val: OHLCVItemState[]) {
    this.draw(val);
  }

  @Input() width: number = 800;
  @Input() height: number = 600;

  constructor(private el: ElementRef) { }

  draw(data: OHLCVItemState[]) {
    // window.d3=d3;
    // window.data=data;
    if (!data || !data.length) return;

    console.log('d3 -> ', d3, data);

    let height = this.height, width = this.width;
    let margin = {top: 20, right: 30, bottom: 30, left: 200};

    // let x = d3.scaleBand()
    //   .domain(d3.timeDay.range(data[0].date, data[data.length - 1].date)
    //     // .filter(d => d.getDay() !== 0 && d.getDay() !== 6))
    //   )
    //   .range([margin.left, width - margin.right])
    //   .padding(0.2);
    let x = d3.scaleTime()
      .domain([data[0].date, data[data.length - 1].date])
      .range([margin.left, width - margin.right]);

    let y = d3.scaleLog()
      .domain([d3.min(data, d => d.low), d3.max(data, d => d.high)])
      .rangeRound([height - margin.bottom, margin.top]);

    // Custom multi-scale time format
    // https://bl.ocks.org/mbostock/4149176
    // https://github.com/d3/d3-time-format#d3-time-format
    let xAxis = g => g
      .attr("class", "axis axis-x")
      .attr("transform", `translate(0,${height - margin.bottom})`)
      .call(d3.axisBottom(x));
      // .call(g => g.select(".domain").remove());

    let yAxis = g => g
      .attr("class", "axis axis-y")
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y)
        .tickFormat(d3.format(".7f"))
        .tickValues(d3.scaleLinear().domain(y.domain()).ticks()))
      .call(g => g.selectAll(".tick line").clone()
        .attr("stroke-opacity", 0.2)
        .attr("x2", width - margin.left - margin.right));
      // .call(g => g.select(".domain").remove());


    d3.select(this.el.nativeElement).select('svg').remove();

    const svg = d3.select(this.el.nativeElement).append('svg')
      .attr('width', width)
      .attr('height', height);

    svg.append("g")
      .call(xAxis);

    svg.append("g")
      .call(yAxis);

    const g = svg.append("g")
      .attr("stroke-linecap", "round")
      .attr("stroke", "black")
      .selectAll("g")
      .data(data)
      .enter().append("g")
      .attr("transform", d => `translate(${x(d.date)},0)`)
      .attr("stroke", d => d.open > d.close ? d3.schemeSet1[0]
        : d.close > d.open ? d3.schemeSet1[2]
          : d3.schemeSet1[8]);

    g.append("line")
      .attr("y1", d => y(d.low))
      .attr("y2", d => y(d.high));

    g.append("line")
      .attr("y1", d => y(d.open))
      .attr("y2", d => y(d.close))
      .attr("stroke-width", width/data.length);
    // .attr("stroke-width", x.bandwidth());

    let formatDate = d3.timeFormat("%B %-d, %Y");
    let formatValue = d3.format(".2f");
    let formatChange = (y0, y1) => d3.format("+.2%")((y1 - y0) / y0);

    g.append("title")
      .text(d => `${formatDate(d.date)}
Open: ${formatValue(d.open)}
Close: ${formatValue(d.close)} (${formatChange(d.open, d.close)})
Low: ${formatValue(d.low)}
High: ${formatValue(d.high)}`);

  }

}
