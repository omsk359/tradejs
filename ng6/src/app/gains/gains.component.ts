import {Component, OnInit, ViewChild} from '@angular/core';
import {timeframeNames} from "../chart/chart.component";
import {AppState, LoadState, OHLCVItemState} from "../reducers";
import {Subscription} from "rxjs/internal/Subscription";
import {Observable} from "rxjs/internal/Observable";
import {ApiService} from "../api.service";
import {Store} from "@ngrx/store";
import {CcxtActionTypes, ExchangesLoad, LoadSuccess, MarketsLoad, OHLCVLoad, TaskDel, TasksListLoad} from "../actions";
import {delay, filter, find, map, mergeMap, pairwise, skip, take, tap} from "rxjs/operators";
import _ from 'lodash';
import * as ccxt from "ccxt";

import * as moment from 'moment';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatSort, MatTable, Sort} from "@angular/material";
import {TaskCreate, TaskDelRequest, TaskSubWS} from "../actions";
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from "@angular/material-moment-adapter";

// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM HH:mm',
  },
  display: {
    dateInput: 'DD.MM HH:mm',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-gains',
  templateUrl: './gains.component.html',
  styleUrls: ['./gains.component.css'],
  providers: [
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
  ]
})
export class GainsComponent implements OnInit {

  timeframes: {value: string, name: string}[] = [];
  timeframe: string;

  exchange: string;
  exchanges: string[] = [];
  market: string;
  markets: {[exchange: string]: string[]} = {};

  exchangesLoad$: Observable<LoadState>;
  marketsLoad$: Observable<LoadState>;

  startDate: any;
  limit: number;

  ccxtSub: Subscription;

  @ViewChild(MatSort) sortTable: MatSort;

  taskId: string;

  taskSub: Subscription;

  tfCalcType: string;
  tfCalcTypes = ['Open/Close', 'High/Low'];

  progressPrec: number = 0;
  isRun = false;

  dataSource;// = [{"_id":"5b38cd69456ad40c80fc3164","exchange":"binance","market":"ETH/BTC","timeframe":"1d","values":[[1529798400000,0.076947,0.077222,0.0727,0.074027,122854.452],[1529884800000,0.074028,0.075563,0.0728,0.073401,102442.288],[1529971200000,0.07346,0.073682,0.070702,0.070826,62548.392],[1530057600000,0.070826,0.072166,0.069956,0.071957,102713.029],[1530144000000,0.07194,0.072189,0.070652,0.071832,94656.26],[1530230400000,0.071832,0.072084,0.0691,0.070112,113758.357],[1530316800000,0.070111,0.07171,0.069935,0.071058,116930.885],[1530403200000,0.071058,0.071536,0.070562,0.07129,44029.959],[1530489600000,0.071199,0.072469,0.070612,0.071989,117288.426],[1530576000000,0.071939,0.072889,0.0714,0.072381,29161.07]]},{"_id":"5b3b118690f2f9640dc69177","exchange":"binance","market":"BNB/ETH","timeframe":"1d","values":[[1529798400000,0.032912,0.03305,0.031,0.031699,273250.13],[1529884800000,0.031691,0.032632,0.030778,0.0325,179242.21],[1529971200000,0.032501,0.032972,0.031777,0.032743,128713.68],[1530057600000,0.032741,0.033106,0.0318,0.031856,148982.83],[1530144000000,0.031859,0.03421,0.031777,0.033629,279374.82],[1530230400000,0.033689,0.034598,0.033065,0.033367,201890.5],[1530316800000,0.033305,0.033387,0.0316,0.032392,178993.39],[1530403200000,0.032353,0.032416,0.031511,0.031701,127740.13],[1530489600000,0.031709,0.031908,0.030111,0.030629,201844.19],[1530576000000,0.03068,0.030707,0.030247,0.030485,47692.3]]}];
  sortedData;// = this.dataSource;//[{"_id":"5b38cd69456ad40c80fc3164","exchange":"binance","market":"ETH/BTC","timeframe":"1d","values":[[1529798400000,0.076947,0.077222,0.0727,0.074027,122854.452],[1529884800000,0.074028,0.075563,0.0728,0.073401,102442.288],[1529971200000,0.07346,0.073682,0.070702,0.070826,62548.392],[1530057600000,0.070826,0.072166,0.069956,0.071957,102713.029],[1530144000000,0.07194,0.072189,0.070652,0.071832,94656.26],[1530230400000,0.071832,0.072084,0.0691,0.070112,113758.357],[1530316800000,0.070111,0.07171,0.069935,0.071058,116930.885],[1530403200000,0.071058,0.071536,0.070562,0.07129,44029.959],[1530489600000,0.071199,0.072469,0.070612,0.071989,117288.426],[1530576000000,0.071939,0.072889,0.0714,0.072381,29161.07]]},{"_id":"5b3b118690f2f9640dc69177","exchange":"binance","market":"BNB/ETH","timeframe":"1d","values":[[1529798400000,0.032912,0.03305,0.031,0.031699,273250.13],[1529884800000,0.031691,0.032632,0.030778,0.0325,179242.21],[1529971200000,0.032501,0.032972,0.031777,0.032743,128713.68],[1530057600000,0.032741,0.033106,0.0318,0.031856,148982.83],[1530144000000,0.031859,0.03421,0.031777,0.033629,279374.82],[1530230400000,0.033689,0.034598,0.033065,0.033367,201890.5],[1530316800000,0.033305,0.033387,0.0316,0.032392,178993.39],[1530403200000,0.032353,0.032416,0.031511,0.031701,127740.13],[1530489600000,0.031709,0.031908,0.030111,0.030629,201844.19],[1530576000000,0.03068,0.030707,0.030247,0.030485,47692.3]]}];
  // dataSource$: Observable<any>;// = of(this.dataSource).pipe(delay(4000));

  tasks$: Observable<any>;

  constructor(private store: Store<AppState>, private api: ApiService) {
    this.tasks$  = api.tasks$.pipe(map(obj => _.map(obj, task => task)));
  }

  ngOnInit() {
    // this.exchange = 'btcmarkets';
    this.exchange = 'binance';
    this.loadMarkets(this.exchange);
    this.timeframe = '1d';
    // this.timeframes = timeframeNames;
    this.limit = 10;
    this.startDate = moment.utc().add(-this.limit, 'days').startOf('day');
    this.tfCalcType = this.tfCalcTypes[0];

    this.loadExchanges();
    this.ccxtSub = this.api.ccxt$.subscribe(({exchanges, markets}) => {
      this.exchanges = exchanges;
      this.markets = _.mapValues(markets, markets => _.keys(markets));
    });
    this.exchangesLoad$ = this.api.loading$.pipe(map(data => data[CcxtActionTypes.ExchangesLoad]));
    this.marketsLoad$ = this.api.loading$.pipe(map(data => data[CcxtActionTypes.MarketsLoad]));

    this.loadTaskList();
    this.tasks$.pipe(skip(1), take(1), map(tasks => tasks.filter(t => t.type == 'fetchOHLCV'))).subscribe(tasks => {
      console.log('tasks', tasks);
      let runTasks = tasks.filter(t => _.get(t, 'state.taskState') == 'RUN');
      if (runTasks.length == 1) {
        let [task] = runTasks;
        this.timeframe = task.params.timeframe;
        this.isRun = true;
        this.subscribeTask(task._id);
      }
    });
  }

  loadTaskList() {
    this.store.dispatch(new TasksListLoad());
  }

  removeTask(task) {
    if (_.get(task, 'state.taskState') == 'RUN')
      this.isRun = false;
    console.log('removeTask', task, this.isRun);
    this.store.dispatch(new TaskDelRequest(task._id));
  }

  loadExchanges() {
    this.store.dispatch(new ExchangesLoad());
  }

  dateChange(e) {
    // console.log('e', e.target.value);
    this.startDate = moment.utc(e.target.value, MY_FORMATS.parse.dateInput);
  }

  loadMarkets(exchange) {
    let ccxtExchange = new ccxt[exchange];
    console.log('ccxtExchange', ccxtExchange);
    if (ccxtExchange.timeframes) {
      this.timeframes = timeframeNames.filter(({value}) => value in ccxtExchange.timeframes);
      if (!this.timeframes.find(item => item.value === this.timeframe))
        this.timeframe = this.timeframes.length && this.timeframes[0].value;
    } else {
      this.timeframes = [];
      this.timeframe = null;
    }

    if (exchange in this.markets) {
      this.store.dispatch(new LoadSuccess(new MarketsLoad()));  // reset error
      return;
    }
    this.store.dispatch(new MarketsLoad([exchange]));
  }

  tfHeaders() {
    if (!this.dataSource || !this.dataSource.length) return [];
    let dates = this.dataSource[0].values.map(([time]) => time);
    if (['1m', '3m', '5m', '15m', '30m', '1h', '2h'].includes(this.timeframe))
      return _.uniq(dates.map(d => moment.utc(d).format('H:mm')));
    if (['4h', '6h', '8h', '12h'].includes(this.timeframe))
      return _.uniq(dates.map(d => moment.utc(d).format('D/M H:mm')));
    if (['1d', '3d', '1w', '1M'].includes(this.timeframe))
      return _.uniq(dates.map(d => moment.utc(d).format('D/M')));
    return [];
  }
  columns() {
    if (!this.sortedData && !this.isRun) return [];
    return ['market', ...this.tfHeaders()];
  }

  tfValue(values: number[]): number {
    if (!values) return -100000;
    let [time, open, high, low, close, vol] = values;
    if (this.tfCalcType == 'Open/Close')
      return (open-close)/close*100;
    return (high-low)/low*100;
  }

  sortData(sort: Sort) {
    const data = this.dataSource.slice();
    if (!sort.active || sort.direction === '') {
      this.sortedData = data;
      return;
    }

    function compare(a, b, isAsc) {
      return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      let i = this.tfHeaders().findIndex(h => h == sort.active);

      return compare(this.tfValue(a.values[i]), this.tfValue(b.values[i]), isAsc);
    });
  }

  subscribeTask(taskId) {
    console.log('subscribeTask', taskId);
    // setTimeout(() => {
    //   this.store.dispatch(new TaskSubWS(taskId));
    // }, 50);
    // setTimeout(() => {
    //   this.taskSub && this.taskSub.unsubscribe();
    // }, 200);
    this.store.dispatch(new TaskSubWS(taskId));
    this.taskSub && this.taskSub.unsubscribe();
    this.taskSub = this.tasks$.pipe(map(tasks => tasks.find(task => task._id == taskId))).subscribe(task => {
      if (!task) return;
      console.log('subscribeWsTask sub', task);
      this.dataSource = task.state.results.filter(r => !r.error);
      this.sortData({ active: this.sortTable.active, direction: this.sortTable.direction });
      if (task.state.results)
        this.progressPrec = task.state.results.length / task.params.markets.length * 100;
      if (task.state.taskState == 'FINISH') {
        this.isRun = false;
        this.removeTask(task);
      }
    });
  }

  load() {
    let markets = this.markets[this.exchange];
    // markets = markets.slice(0, 2);
    console.log('this.startDate', this.startDate.format());
    // let params = {exchange: this.exchange, markets: markets, timeframe: this.timeframe, startDate: +this.startDate, limit: this.limit, noCache: true};
    let params = {exchange: this.exchange, markets: markets, timeframe: this.timeframe, startDate: +this.startDate, limit: this.limit};

    this.store.dispatch(new TaskCreate({ type: 'fetchOHLCV', params }));
    this.isRun = true;
    this.tasks$.pipe(take(2), pairwise(),
                      map(([oldList, newList]) => _.differenceBy(newList, oldList, task => task._id)[0]._id))
                .subscribe(taskId => this.subscribeTask(taskId));
  }

  ngOnDestroy() {
    this.taskSub && this.taskSub.unsubscribe();
  }
}
