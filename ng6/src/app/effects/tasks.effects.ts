import {from, Observable, of} from "rxjs/index";
import {
  AlgoDel,
  AlgoListLoad, AlgoStateDel, AlgoStateSet, AlgoStateWS,
  LoadFail,
  LoadStart,
  LoadSuccess, LogAppend, LogLoad, LogSet, LogWS, LvlDel, LvlDelReq,
  MarketInfoLoad,
  MarketInfoSet,
  MarketStateSet,
  MarketStateWS,
  MyOrdersLoad,
  MyOrdersSet,
  OrderbookLoad,
  OrderbookSet,
  ShowError,
  TabSet, TaskDel, TasksActionTypes, TaskSet, TasksListLoad,
  TickLoad,
  TickSet,
  TradingActionTypes, TradingResume,
  TradingStart, TradingStop
} from "../actions";
import {catchError, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {Action, Store} from "@ngrx/store";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {HttpClient} from "@angular/common/http";
import {ApiService} from "../api.service";
import {Injectable} from "@angular/core";
import {TaskCreate, TaskDelRequest, TasksListSet, TaskSubWS} from "../actions/tasks.actions";
import {AppState} from "../reducers";
import _ from 'lodash';

@Injectable()
export class TasksEffects {

  @Effect()
  tasksList$: Observable<Action> = this.actions$.pipe(
    ofType<TasksListLoad>(TasksActionTypes.TasksListLoad),
    tap(action => this.store.dispatch(new LoadStart(action))),
    switchMap(action =>
      this.api.taskList().pipe(
        // switchMap(list => from(list.map(result => new TaskSet(result)))),
        map(list => new TasksListSet(list)),
        tap(() => this.store.dispatch(new LoadSuccess(action))),
        catchError(error => of(new LoadFail({...action, error})))
      )
    )
  );

  @Effect()
  taskDel$: Observable<Action> = this.actions$.pipe(
    ofType<TaskDelRequest>(TasksActionTypes.TaskDelRequest),
    tap(action => this.store.dispatch(new LoadStart(action))),
    switchMap(action =>
      this.api.taskDel(action.taskId).pipe(
        map(({ok}) => {
          if (!ok) throw new Error(`Removing ${action.taskId} failed`);
          return new TaskDel(action.taskId);
        }),
        tap(() => this.store.dispatch(new LoadSuccess(action))),
        catchError(error => from([new ShowError(error), new LoadFail({type: action.type, error})]))
      )
    )
  );

  @Effect()
  taskCreate$: Observable<Action> = this.actions$.pipe(
    ofType<TaskCreate>(TasksActionTypes.TaskCreate),
    tap(action => this.store.dispatch(new LoadStart(action))),
    switchMap(action =>
      this.api.createTask(action.payload).pipe(
        map(task => new TaskSet(task)),
        // switchMap(task => from([new TaskSet(task), new TaskSubWS(task._id)])),
        tap(() => this.store.dispatch(new LoadSuccess(action))),
        catchError(error => from([new ShowError(error), new LoadFail({type: action.type, error})]))
      )
    )
  );

  @Effect()
  taskSubWS$: Observable<Action> = this.actions$.pipe(
    ofType<TaskSubWS>(TasksActionTypes.TaskSubWS),
    // tap(action => this.store.dispatch(new LoadStart(action))),
    switchMap(action =>
      this.api.subscribeWsTask(action.taskId).pipe(
        tap(a => {
          console.log('azxz', a);
        }),
        switchMap(task => {
          let err = _.chain(task).get('state.results').last().get('error').value();
          if (err) return from([new ShowError(err), new TaskSet(task)]);
          return of(new TaskSet(task));
        }),
        // map(task => {
        //   let err = _.chain(task).get('state.results').last().get('error').value();
        //   // if (err) return new ShowError(err);
        //   return err ? new ShowError(err) : new TaskSet(task);
        // }),
        // tap(() => this.store.dispatch(new LoadSuccess(action))),
        catchError(error => of(new ShowError(error)))
      )
    )
  );

  constructor(private http: HttpClient, private actions$: Actions, private api: ApiService, private store: Store<AppState>) {}
}
